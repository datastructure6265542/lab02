public class RemoveDuplicates {
    public static int remove(int[] x) {
        if (x.length == 0) {
            return 0;
        }
        
        int k = 1; // Initialize the count of unique elements
        
        for (int i = 1; i < x.length; i++) {
            if (x[i] != x[i - 1]) {
                x[k] = x[i]; // Move the unique element to the next position
                k++;
            }
        }
        
        return k;
    }
    
    public static void main(String[] args) {
        
        int[] nums1 = {1, 1, 2};
        int k1 = remove(nums1);
        System.out.println("Output: " + k1); // Output: 2
        // Print the modified nums1 array
        for (int i = 0; i < k1; i++) {
            System.out.print(nums1[i] + " ");
        }
        System.out.println();
        
        int[] nums2 = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        int k2 = remove(nums2);
        System.out.println("Output: " + k2); // Output: 5
        // Print the modified nums2 array
        for (int i = 0; i < k2; i++) {
            System.out.print(nums2[i] + " ");
        }
        System.out.println();
    }
}
